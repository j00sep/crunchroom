defmodule Vueshatter.Repo.Migrations.Ingredients do
  use Ecto.Migration

  def change do
    create table(:ingredients) do
      add :name, :string
      add :count, :integer
      timestamps()
    end
    create unique_index(:ingredients, [:name])
  end

end
