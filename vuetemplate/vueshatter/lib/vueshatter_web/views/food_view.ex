defmodule VueshatterWeb.FoodView do
  use VueshatterWeb, :view
  alias VueshatterWeb.FoodView

  def render("index.json", %{combos: combos}) do
    %{data: render_many(combos, FoodView, "foodrecipe.json")}
  end

  def render("show.json", %{combos: combos}) do
    %{data: render_one(combos, FoodView, "foodrecipe.json")}
  end

end
