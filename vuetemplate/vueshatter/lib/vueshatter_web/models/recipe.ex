defmodule Vueshatter.Models.Recipe do
  use Ecto.Schema
  import Ecto.Changeset
  @derive {Poison.Encoder, only: [:id, :name, :method]}

  schema "recipes" do
      field :name, :string
      field :method, :string
      field :cooking_time, :string
      field :dietindex_id, :integer
      has_many :mappings,  Vueshatter.Models.Mapping
    timestamps()
  end

  @doc false
  def changeset(recipe, attrs) do
    recipe
    |> cast(attrs, [:name, :method, :cooking_time, :dietindex_id])
    |> validate_required([:name, :method, :cooking_time, :dietindex_id])
  end
end
