defmodule Vueshatter.Models.Dietindex do
  use Ecto.Schema
  import Ecto.Changeset
  @derive {Poison.Encoder, only: [:id, :name]}


  schema "dietindex" do
    field :name, :string
    timestamps()
  end

  @doc false
  def changeset(diet, attrs) do
    diet
    |> cast(attrs, [:name])
    |> validate_required([:name])
  end
end
