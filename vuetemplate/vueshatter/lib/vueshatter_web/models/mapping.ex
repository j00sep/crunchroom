defmodule Vueshatter.Models.Mapping do
  use Ecto.Schema
  import Ecto.Changeset
  alias Vueshatter.Models.Recipe
  alias Vueshatter.Models.Ingredient
  @derive {Poison.Encoder, only: [:recipe]}

  schema "mappings" do
    belongs_to :ingredient, Ingredient
    belongs_to :recipe, Recipe
    timestamps()
  end

  @doc false
  def changeset(mapping, attrs) do
    mapping
    |> cast(attrs, [:ingredient_id,  :recipe_id])
    |> validate_required([:ingredient_id, :recipe_id])
  end
end

