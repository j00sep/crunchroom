defmodule Vueshatter.Models.Ingredient do
  use Ecto.Schema
  import Ecto.Changeset
  @derive {Poison.Encoder, only: [:id, :name, :count]}


  schema "ingredients" do
    field :name, :string
    field :count, :integer
    has_many :mappings,  Vueshatter.Models.Mapping
    timestamps()
  end

  @doc false
  def changeset(ingredient, attrs) do
    ingredient
    |> cast(attrs, [:name, :count])
    |> validate_required([:name, :count])
  end
end
