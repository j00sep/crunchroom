defmodule VueshatterWeb.FoodController do
  use VueshatterWeb, :controller
  alias Vueshatter.DataAccess
  alias Vueshatter.Repo

  def index(conn, _params) do
    render conn, "index.html"
  end

  def getbuyitem(conn, _params) do
    itemm_to_buy = DataAccess.itemtobuy!()|>Repo.all
    IO.puts("--------")
    IO.inspect(itemm_to_buy)
    IO.puts("--------")
    conn
    |> put_status(:ok)
    |> json(itemm_to_buy)
  end

  def foodselect(conn, %{"items" => params}) do
    # IO.puts("==========")
    # IO.inspect(params)
    # IO.puts("==========")
    combos=for item <- params do
      result = DataAccess.get_item_name!(item["name"])|>Repo.all|>List.first
      DataAccess.get_mappings!(result.id)|> Repo.all|>Repo.preload(:recipe)|>Enum.uniq
    end


    conn
    |> put_status(:ok)
    |> json(combos)
  end

  def details(conn, %{"recipeId" => params }) do
    result =  DataAccess.get_recipe!(params["id"])|>Repo.one!
    resultmap = %{:recipe => result.name, :cooking_method => result.method}
    conn
    |> put_status(:ok)
    |> json(resultmap)
  end

  def getallindex(conn, %{}) do
    result =  DataAccess.list_dietindex
    IO.puts("--------")
    IO.inspect(result)
    IO.puts("--------")
    conn
    |> put_status(:ok)
    |> json(result)
  end

  def searchrecipe(conn, %{"index" => params}) do
    # values = DataAccess.search(params["names"], params["dietindex_id"], params["cooking_time"])|>Repo.all|>Enum.uniq
    # |> Map.new

    # IO.puts("--------")
    # IO.inspect(values)
    # IO.puts("--------")
      # l.id,l.name
    combos=for item <- params["names"] do
      result = DataAccess.get_item_name!(item)|>Repo.all|>List.first
      DataAccess.get_mappings!(result.id)|> Repo.all|>Repo.preload(:recipe)|>Repo.preload(:ingredient)
    end
    conn
    |> put_status(:ok)
    |> json(combos)
  end


end


