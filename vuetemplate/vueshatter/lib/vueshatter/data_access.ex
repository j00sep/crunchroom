defmodule Vueshatter.DataAccess do
  @moduledoc """
  The Accounts context.
  """
  import Ecto.Query, warn: false
  alias Vueshatter.Repo

  # alias Vueshatter.Models.LoanHistory
  alias Vueshatter.Models.Recipe
  alias Vueshatter.Models.Ingredient
  alias Vueshatter.Models.Mapping
  alias Vueshatter.Models.Dietindex



  def list_dietindex do
    Repo.all(Dietindex)
  end

  def searchIndex() do

  end

  def add_recipe(attrs \\ %{}) do
    %Recipe{}
    |> Recipe.changeset(attrs)
    |> Repo.insert()
  end

  def add_Ingredients(attrs \\ %{}) do
    %Ingredient{}
    |> Ingredient.changeset(attrs)
    |> Repo.insert()
  end

  def create_mappings(attrs \\ %{}) do
    %Mapping{}
    |> Mapping.changeset(attrs)
    |> Repo.insert()
  end

  def create_dietindex(attrs \\ %{}) do
    %Dietindex{}
    |> Dietindex.changeset(attrs)
    |> Repo.insert()
  end
  # def get_recipes!(ingre_id) do
  #   #from(driver_loc in query, where: driver_loc.status == "Invisible", order_by: fragment("? <-> ST_SetSRID(ST_MakePoint(?,?), ?)", driver_loc.driver_lat_long, ^lng, ^lat, ^srid))
  #   from orders  in Mapping , where: orders.ingredients_id == ^ingre_id
  #  end

  # SELECT * from public.recipes r
	# inner join public.mappings m  on  r.id = m.recipe_id
	# inner join public.ingredients i on i.id = m.ingredient_id
  def search(ing_name, dietindex, time) do
    from l in Recipe,
    join: b in Mapping, on: l.id == b.recipe_id,
    join: i in Ingredient, on: i.id == b.recipe_id,
    where: i.name in ^ing_name and l.dietindex_id in ^dietindex and l.cooking_time in ^time,
    select: {l.name, l.id}
  end


  def update_items(%Ingredient{} = ingredient, attrs) do
    ingredient
    |> Ingredient.changeset(attrs)
    |> Repo.update()
  end
   def  itemtobuy!() do
    from items in Ingredient, where: items.count == 0
   end
   def  get_item_name!(name) do
     from items in Ingredient, where: items.name == ^name
    end

  def get_mappings!(ingr_id) do
  from orders  in Mapping, where: orders.ingredient_id == ^ingr_id
  end

  def get_recipe!(id) do
    from orders  in Recipe, where: orders.id == ^id
    end

  def get_recipes!(id), do: Repo.get!(Recipe, id)
  # def get_recipe!(ingredients) do
  #  from items in Recipe, where:  items.ingredients == fragment("ANY(?)", ^ingredients)

  # end

  # def getvalue(id) do
  #   from item in Recipe, where: item.id == ^id
  # end

end
