defmodule Vueshatter.Repo.Migrations.Recipe do
  use Ecto.Migration

  def change do
    create table(:recipes) do
      add :name, :string
      add :method, :string
      add :cooking_time, :string
      add :dietindex_id, :integer
      timestamps()
  end
  create unique_index(:recipes, [:name])
end
end
