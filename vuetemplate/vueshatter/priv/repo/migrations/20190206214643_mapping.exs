defmodule Vueshatter.Repo.Migrations.Mapping do
  use Ecto.Migration


    def change do
      create table(:mappings) do
      add :ingredient_id, references(:ingredients)
      add :recipe_id, references(:recipes)
      timestamps()
    end
    create index(:mappings, [:ingredient_id])
    create index(:mappings, [:recipe_id])
  end

end



