defmodule Vueshatter.Repo.Migrations.Dietindex do
  use Ecto.Migration

  def change do
    create table(:dietindex) do
      add :name, :string
      timestamps()
  end
end
end
