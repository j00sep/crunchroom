# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     Vueshatter.Repo.insert!(%Vueshatter.SomeSchema{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.
alias Vueshatter.DataAccess

ingredients = [
  %{
    name: "Potato",
    count: 1
  },
  %{
    name: "Milk",
    count: 7
  },
  %{
    name: "Kefir",
    count: 0
  },
  %{
      name: "Ice cream",
      count: 7

  },
  %{
      name: "Cream",
      count: 7

  },
  %{
    name: "Ketchup",
    count: 7
},
%{
  name: "Strawberry Jam",
  count: 7
},


%{
  name: "Chicken Breast",
  count: 7
},

%{
  name: "Paprika",
  count: 7
}
]
Enum.each(ingredients, fn(data) ->
DataAccess.add_Ingredients(data)
end)



food =[
%{
  name: "Chicken Parika",
  method: "fry chicken and put some parika",
  cooking_time: "1 to 2 hours",
  dietindex_id: 3
},
  %{
  name: "StrawBerry Jam Icecream",
  method: "This is the most easy to cook,Put in some Jam and mix the ice",
  cooking_time: "10 to 30 minutes",
  dietindex_id: 2
},
%{
  name: "Fries",
  method: "Just fry your potatoes",
  cooking_time: "10 to 30 minutes",
  dietindex_id: 1
},
]

  Enum.each(food, fn(data) ->
      DataAccess.add_recipe(data)
    end)


    maps = [
      %{
        ingredient_id: 8,
        recipe_id: 1,
        #"Chicken Parika",

      },
      %{
        ingredient_id: 9,
        recipe_id: 1,
        #"Chicken Parika",
      },
      %{
        ingredient_id: 2,
        recipe_id: 2
        #"StrawBerry Jam Icecream",
      },
      %{
        ingredient_id: 4,
        recipe_id: 2
        #"StrawBerry Jam Icecream",
      },
      %{
        ingredient_id: 7,
        recipe_id: 2
        #"StrawBerry Jam Icecream",
      },
      %{
        ingredient_id: 1,
        recipe_id: 3
        #fries
        }
    ]

    Enum.each(maps, fn(data) ->
      DataAccess.create_mappings(data)
        end)


dietindex = [
          %{
            name: "Gluten free"
          },
          %{
            name: "Vegetarian"
          },
          %{
            name: "Dairy free"
          }
        ]

        Enum.each(dietindex, fn(data) ->
          DataAccess.create_dietindex(data)
            end)
